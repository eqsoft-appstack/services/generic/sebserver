# all build-args needs to be definded with --build-arg (default empty values will not build!)

ARG BASE_IMAGE=
ARG BASE_TAG=

FROM ${BASE_IMAGE}:${BASE_TAG}
LABEL maintainer="Stefan Schneider <eqsoft4@gmail.com>"

USER root

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Berlin
SHELL ["/bin/bash", "-c"]

RUN <<EOF
set -e
apt-get update
apt-get install -y --no-install-recommends \
tzdata \
apt-transport-https \
ca-certificates \
software-properties-common \
curl \
gnupg \
nano
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
echo $TZ > /etc/timezone
curl -sL https://deb.nodesource.com/setup_current.x | bash -
apt-get update
apt-get -y install nodejs
EOF

ARG SEBSERVER_PORT=8441
ENV SEBSERVER_PORT=$SEBSERVER_PORT

# replace with new default sebserver code after testing
COPY ./files/ /opt/

RUN chown -R www-data:root /opt/server \
&& chmod -R 755 /opt/server

USER www-data

WORKDIR /opt/server
CMD ["/usr/bin/node", "server.js"]
