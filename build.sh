#!/usr/bin/env bash

set -euo pipefail

ARG=${1:-}

TEST_RUN=0

if [[ "$ARG" == "-t" ]] || [[ "$ARG" == "--test-run" ]] ; then
    TEST_RUN=1
fi

FORCE=0

if [[ "$ARG" == "-f" ]] || [[ "$ARG" == "--force" ]] ; then
    FORCE=1
fi

. ./env

# get current repo HEAD
if [ $IS_LOCAL == 1 ] ; then
    git pull
fi

# only in prod run
if [ $TEST_RUN != 1 ] ; then
    . ./check_version.sh
fi

# for multiarch builds
docker run --rm --userns=host --privileged multiarch/qemu-user-static --reset -p yes

# in local environment i want to use always the same builder
# in gitlab SaaS shared runner always a fresh instance will be spawned
set +e
CI_BUILDER_EXISTS=$(docker buildx ls | grep cibuilder)
set -e

# in remote environments there is no builder instance 
if [ -z "${CI_BUILDER_EXISTS}" ] ; then
    docker buildx create --name=cibuilder --driver docker-container --use
fi

docker buildx inspect --bootstrap

# building image for testing, the image will be re-used from buildkit cache in final build, 
# so no problems with redundant multiple builds

# building test image and export to local docker image storage
docker buildx build \
    --build-arg BASE_IMAGE=$BASE_IMAGE \
    --build-arg BASE_TAG=$BASE_TAG \
    --output=type=docker \
    --tag $TARGET_IMAGE:$BASE_TAG \
    --file ./Containerfile \
    .

if [ -x "test.sh" ] ; then
    . ./test.sh
fi

# stop here if TEST_RUN = 1
if [ $TEST_RUN == 1 ] ; then
    exit 0
fi

if [ -z "${DOCKER_TOKEN:-}" ] ; then    # for local
    docker login
else                                  # for remote
    echo "$DOCKER_TOKEN" | docker login -u $DOCKER_USER --password-stdin
fi

if [ $AUTOMATED_MINOR_TAG == 1 ] ; then
    # final build and push
    if [ ! -f "./minor_tag.sh" ] ; then
        echo "missing minor_tag.sh script"
        exit 1
    fi
    . ./minor_tag.sh
    docker buildx build \
        --platform $PLATFORMS \
        --build-arg BASE_IMAGE=$BASE_IMAGE \
        --build-arg BASE_TAG=$BASE_TAG \
        --tag $TARGET_IMAGE:$BASE_TAG \
        --tag $TARGET_IMAGE:$MINOR_TAG \
        --file ./Containerfile \
        --push \
        .
else
    docker buildx build \
        --platform $PLATFORMS \
        --build-arg BASE_IMAGE=$BASE_IMAGE \
        --build-arg BASE_TAG=$BASE_TAG \
        --tag $TARGET_IMAGE:$BASE_TAG \
        --file ./Containerfile \
        --push \
        .
fi

. ./git_push.sh

