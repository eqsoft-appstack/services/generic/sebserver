

REPO_TAGS=$(skopeo inspect docker://$BASE_IMAGE:$BASE_TAG | jq '.RepoTags' )

# sort all tags with highest number on top and filter all tags with minor tag format
# this will reduce the required requests for getting a match
# the minortag regex might be different between repos (like "^v\\d+\\.\\d+\\.\\d+$" or ^\\d+\\.\\d+\\.\\d+.*$)

MINOR_TAGS=$(jq -r 'reverse | .[] | select( . | test("^\\d+\\.\\d+-slim$"))' <(echo $REPO_TAGS))

MINOR_TAG=

for MINOR_TAG in $MINOR_TAGS
do
    digest=$(skopeo inspect --no-tags --format '{{ .Digest }}' docker://$BASE_IMAGE:$MINOR_TAG)
    echo "$digest - $MINOR_TAG"
    if [ "$digest" == "$CURRENT_DIGEST" ] ; then
        echo "found matching tag for $BASE_TAG = $MINOR_TAG with same digest $digest"
        break
    fi
    MINOR_TAG=
done

if [ -z "${MINOR_TAG}" ] ; then
    echo "could not get MINOR_TAG from $BASE_TAG"
    exit 1
fi