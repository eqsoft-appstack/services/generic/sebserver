# testing nginx from local docker storage

echo "running test-image"
# switch the TRAP off
set +e

TEST_IMAGE=test-image-$RANDOM

docker run -d --rm --name $TEST_IMAGE $TARGET_IMAGE:$BASE_TAG

# required?
sleep 5

docker ps -a

ret=$(docker logs $TEST_IMAGE 2>&1 | grep "Websocket for monitoring started on port")

ret_code=$?

echo "return code: $ret_code"

# something went wrong
if [ ! "$ret_code" == "0" ] ; then
    echo "testing failed!"
    docker rm -f $TEST_IMAGE &>/dev/null
    # switch the TRAP on and exit the pipeline
    set -e
    exit 1
else 
    # everything ok
    echo "testing ok!"
    docker rm -f $TEST_IMAGE &>/dev/null
    # switch the TRAP on
    set -e
fi
